use std::fs;

/// Gets all unique answers, no repetitions
fn all_ans(ans: &str) -> Vec<char> {
    let mut chrs = Vec::new();

    // Puts all characters in chrs
    for chr in ans.chars() {
        if !chrs.contains(&chr) && chr.is_alphanumeric() {
            chrs.push(chr);
        }
    }

    chrs
}

/// Gets the answers submitted by everyone in the group
fn unanimous_ans(ans: &str, chrs: Vec<char>) -> Vec<char> {
    let people = ans.lines().count();
    let mut result: Vec<char> = Vec::new();
    // Count answer in chrs
    for chr in chrs {
        if ans.matches(chr).count() == people {
            result.push(chr);
        }
    }

    result
}

fn main() {
    let contents = fs::read_to_string("input.txt").expect("Could not read input");

    let mut total = 0;
    let mut total2 = 0;

    for ans in contents.split("\n\n") {
        let chrs = all_ans(ans);
        let shared = unanimous_ans(ans, chrs.clone());
        total += &chrs.len();
        total2 += &shared.len();
    }

    println!("Part one:\n{} things", total);
    println!("Part two:\n{} things", total2);
}
