extern crate regex;
use std::fs;
use std::str::FromStr;
use regex::Regex;

struct Entry {
    lower: u8,
    upper: u8,
    letter: char,
    password: String,
}

impl FromStr for Entry {
    type Err = std::string::ParseError;

    fn from_str(entry: &str) -> Result<Self, Self::Err> {
        // Get occurence range
        let num_re = Regex::new(r"(\d{1,2})-(\d{1,2})").unwrap();
        let numbers = num_re.captures(entry).unwrap();
        let lower: u8 = numbers.get(1).unwrap().as_str().parse::<u8>().unwrap();
        let upper: u8 = numbers.get(2).unwrap().as_str().parse::<u8>().unwrap();

        // Get password and required character
        let char_re = Regex::new(r"([a-z]+): ([a-z]+)").unwrap();
        let chars = char_re.captures(entry).unwrap();
        let letter = chars.get(1).unwrap().as_str().chars().next().unwrap();
        let password = String::from(chars.get(2).unwrap().as_str());

        Ok(Entry { lower, upper, letter, password })
    }
}

fn main() {
    // Set up and parse input
    let input = fs::read_to_string("input.txt").expect("Could not read input");
    let mut entries = Vec::new();
    for line in input.lines() {
        entries.push(Entry::from_str(line).unwrap());
    }

    println!("Part one:");
    let mut valid: u32 = 0;
    for entry in &entries {
        let matches = entry.password.as_str().matches(entry.letter).count();

        if matches >= entry.lower as usize && matches <= entry.upper as usize {
            valid += 1;
        }
    }
    println!("Found {} valid passwords.", valid);

    println!("\nPart two:");
    let mut count: u32 = 0;
    for entry in &entries {
        let first = entry.password.as_bytes()[entry.lower as usize - 1];
        let second = entry.password.as_bytes()[entry.upper as usize - 1];
        let mut valid: bool = false;
       
        if entry.letter == first as char {
            valid = !valid;
        }
        if entry.letter == second as char {
            valid = !valid;
        }
        if valid {
            count += 1;
        }
    }
    println!("Found {} valid passwords.", count);
}
