use std::fs;

fn sled(map: &[Vec<char>], slope_right: usize, slope_down: usize) -> u32 {
    let mut trees: u32 = 0;
    let mut x = 0;

    for y in (0..map.len()).step_by(slope_down) {
        if map[y][x] == '#' {
            trees += 1;
        }

        // Move and wrap around, as because map repeats horizontally
        x = (x + slope_right) % map[0].len();
    }

    trees
}


fn main() {
    // Load map
    let input = fs::read_to_string("input.txt").expect("Could not read input");
    let mut map: Vec<Vec<char>> = vec![];
    for line in input.lines() {
        let row: Vec<char> = line.chars().collect();
        map.push(row);
    }

    println!("Part one:");
    println!("Encountered {} trees", sled(&map, 3, 1));

    println!("\nPart two:");
    let product = sled(&map, 1, 1) * sled(&map, 3, 1) * sled(&map, 5, 1) * sled(&map, 7, 1) * sled(&map, 1, 2);
    println!("\nProduct of slopes = {}", product);
}
