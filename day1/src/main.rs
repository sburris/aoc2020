use std::fs;

fn main() {
    // Load input into a vec
    let input = fs::read_to_string("input.txt").expect("Could not read input");
    let mut expenses: Vec<u64> = Vec::new();
    
    for line in input.lines() {
        expenses.push(line.parse().expect("Could not parse input"));
    }

    // Part one
    println!("Part one:");
    'outer: for i in &expenses {
        for j in &expenses {
            if i + j == 2020 {
                println!("{} + {} = 2020!", i, j);
                println!("{} × {} = {}", i, j, i * j);
                break 'outer;
            }
        }
    }

    // Part two
    println!("\nPart two:");
    'outer2: for i in &expenses {
        for j in &expenses {
            for k in &expenses {
                if i + j + k == 2020 {
                    println!("{} + {} + {} = 2020!", i, j, k);
                    println!("{} × {} × {} = {}", i, j, k, i*j*k);
                    break 'outer2;
                }
            }
        }
    }
}
