use std::fs;

// Part one
fn check_passport(passport: &str) -> bool {
    let req_fields = vec!["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];
    for field in req_fields {
        if !passport.contains(field) {
            return false
        }
    }
    true
}

// Part two
fn check_range(val: &str, lower: u32, upper: u32) -> bool {
    let val = val.parse::<u32>().expect("Could not parse");
    val >= lower && val <= upper
}

fn check_height(val: &str) -> bool {
    let len = val.len();
    if len > 2 {
        let num = &val[0..len-2];

        match &val[len-2..] {
            "cm" => check_range(num, 150, 193),
            "in" => check_range(num, 59, 76),
            _ => false
        }
    }
    else {
        false
    }
}

fn check_eyes(val: &str) -> bool {
    ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].contains(&val)
}

fn check_hair(val: &str) -> bool {
    let hex_chars = ['#', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];
    if !val.contains('#') {
        return false
    }

    for chr in val.chars() {
        if !hex_chars.contains(&chr) {
            return false
        }
    }
    true
}

fn check_pid(val: &str) -> bool {
    for chr in val.chars() {
        if !chr.is_numeric() {
            return false
        }
    }
    val.len() == 9 
}

fn scrutinize_passport(passport: &str) -> bool {
    let mut results = Vec::new();

    for field in passport.split_whitespace() {
        let field_type = &field[0..=2];
        let val = &field[4..];

        match field_type {
            "byr" => results.push(check_range(val, 1920, 2002)), 
            "iyr" => results.push(check_range(val, 2010, 2020)),
            "eyr" => results.push(check_range(val, 2020, 2030)),
            "hgt" => results.push(check_height(val)),
            "hcl" => results.push(check_hair(val)),
            "ecl" => results.push(check_eyes(val)),
            "pid" => results.push(check_pid(val)),
            "cid" => (),
            _ => ()
        }
    }

    !results.contains(&false)
}

fn main() {
    // Load input
    let input = fs::read_to_string("input.txt").expect("Could not read file.");
    let passports = input.split("\n\n");

    let mut complete = 0;
    let mut valid = 0;
    for passport in passports {
        if check_passport(passport) {
            complete += 1;

            // Check more carefully if all fields are present
            if scrutinize_passport(passport) {
                valid +=1 
            };
        }
    }

    println!("Part one:\nFound {} complete passports.", complete);
    println!("Part two:\nFound {} valid passports.", valid);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_byr() {
        assert_eq!(check_range("2002", 1920, 2002), true);
        assert_eq!(check_range("2003", 1920, 2002), false);
    }

    #[test]
    fn test_hgt() {
        assert_eq!(check_height("60in"), true);
        assert_eq!(check_height("190cm"), true);
        assert_eq!(check_height("190in"), false);
        assert_eq!(check_height("190"), false);
    }

    #[test]
    fn test_ecl() {
        assert_eq!(check_eyes("brn"), true);
        assert_eq!(check_eyes("wat"), false);
    }

    #[test]
    fn test_pid() {
        assert_eq!(check_pid("000000001"), true);
        assert_eq!(check_pid("0123456789"), false);
    }
}
