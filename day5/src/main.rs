use std::fs;

fn search(pass: &str, lower: u32, upper: u32) -> u32 {
    let mut rows = (lower, upper);

    for chr in pass.chars() {
        let mid = (rows.0 + rows.1 + 1) / 2;

        match chr {
            'F' | 'L' => rows.1 = mid,
            'B' | 'R' => rows.0 = mid,
            _ => ()
        }
    }

    rows.0
}

fn find_missing(ids: Vec<u32>) -> Option<u32> {
    for (i, id) in ids.iter().enumerate() {
        if ids[i + 1] != id + 1 {
            return Some(id + 1)
        }
    }
    None
}

fn main() {
    let passes = fs::read_to_string("input.txt").expect("Could not read file");
    let mut ids = Vec::new();

    for pass in passes.lines() {
        let row = search(&pass[0..7], 0, 127);
        let seat = search(&pass[7..], 0, 7);

        ids.push(row * 8 + seat);
    }

    ids.sort_unstable();
    println!("Part one:\nLargest seat ID {:?}", ids[ids.len() - 1]);
    println!("Part two:\nSeat ID {:?}", find_missing(ids).unwrap());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_row() {
        assert_eq!(search("FBFBBFF", 0, 127), 44);
        assert_eq!(search("RLR", 0, 7), 5);
    }
}
