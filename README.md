# Advent of Code 2020
These are my solutions for [AoC2020](https://adventofcode.com/2020) written in the [Rust programming language](https://www.rust-lang.org/).
The directory for each day contains the puzzle input and benchmarks that I use to optimize my solutions.
